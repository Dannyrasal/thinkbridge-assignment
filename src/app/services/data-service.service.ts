import { Injectable } from '@angular/core';
import { Product } from '../models/product'
@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  products: Product[] = [
    new Product(1, "Product 1", "this is the description of product 1", 150),
    new Product(2, "Product 2", "this is the description of product 2", 250),
    new Product(3, "Product 3", "this is the description of product 3", 350),
    new Product(4, "Product 4", "this is the description of product 4", 450),
    // new Product(5, "Product 5", "this is the description of product 5", 550),
    // new Product(6, "Product 6", "this is the description of product 6", 650),
    // new Product(7, "Product 7", "this is the description of product 7", 750),
    // new Product(8, "Product 8", "this is the description of product 8", 850),
  ]
  constructor() { }

  getProducts() {
    return this.products
  }

  addItem(data) {
    console.log(data);
    this.products.push(data);
  }

  viewItem(id){
    console.log(id);
    for (let i = 0; i < this.products.length; i++) {
      if (this.products[i].id == id) {
        return this.products[i]
      }
    }
  }

  removeItem(id) {
    for (let i = 0; i < this.products.length; i++) {
      if (this.products[i].id === id) {
        this.products.splice(i, 1)
      }
    }
    return this.products
  }
}
