import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { DataServiceService } from 'src/app/services/data-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.css']
})
export class CreateItemComponent implements OnInit {
  itemForm : FormGroup;
  submitted = false;
  url: string | ArrayBuffer = "./../../assets/images/logo.png";
  file : File
  fd: FormData;
  image: any;
  constructor(
    private fb:FormBuilder,
    private http:DataServiceService,
    private router: Router) { }

  ngOnInit() {
    this.itemForm = this.fb.group({
      'itemName': ['',Validators.required],
      'description': ['',Validators.required],
      'price': ['',Validators.required],
      'imageUrl':['']
    })
  }

  get item() { return this.itemForm.controls; }

  onSelectFile(event){
    console.log(event.target.files);
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0])
    reader.onload = event => {
      this.url = reader.result;
    };
    this.image = event.target.files[0].name
    console.log(this.itemForm.value.image)
  }

  onSubmit(){
    this.submitted = true;
    if (this.itemForm.invalid) {
        return;
    }else{
      // this.fd = new FormData
      // Object.entries(this.itemForm.value).forEach(
      //   ([key, value]: any[]) => {
      //     this.fd.append(key, value);
      //   });
      //   console.log(this.fd)
      if(this.image == undefined){
        this.itemForm.value.imageUrl = "./../../assets/images/logo.png"
      }else{
        this.itemForm.value.imageUrl = "./../../assets/images/"+this.image
      }
        this.http.addItem(this.itemForm.value)
        this.router.navigate(['/']);
      alert('SUCCESS!! :-)')
    }
  }

}
