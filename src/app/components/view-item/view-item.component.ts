import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataServiceService } from 'src/app/services/data-service.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.css']
})
export class ViewItemComponent implements OnInit {
  itemForm : FormGroup;
  
  url: string | ArrayBuffer = "./../../assets/images/logo.png";
  file : File
  id
  view
  constructor(
    private fb:FormBuilder,
    private http:DataServiceService,
    private router: Router,
    private route : ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id']; 
    console.log(this.id)
    this.itemForm = this.fb.group({
      'itemName': [''],
      'description': [''],
      'price': [''],
      'imageUrl':['']
    })
    this.getValue()
  }

  getValue(){
    this.view = this.http.viewItem(this.id)
    console.log(this.view,this.id);
    this.url=this.view.imageUrl
    this.itemForm.controls['description'].setValue(this.view.description);
    //this.itemForm.controls['imageUrl'].setValue(this.view.imageUrl);
    this.itemForm.controls['itemName'].setValue(this.view.itemName);
    this.itemForm.controls['price'].setValue(this.view.price);
  }
  

}
