import { Component, OnInit } from '@angular/core';
import { DataServiceService } from 'src/app/services/data-service.service';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  products: Product[] = []
  constructor(private dataService: DataServiceService) { }
  ngOnInit() {
    this.products = this.dataService.getProducts()
  }

}
