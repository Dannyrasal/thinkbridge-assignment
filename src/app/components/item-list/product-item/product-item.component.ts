import { Component, OnInit,Input } from '@angular/core';
import { Product } from 'src/app/models/product';
import { DataServiceService } from 'src/app/services/data-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {
  @Input() products = Product
  
  constructor(private dataService:DataServiceService, private router:Router) { }

  ngOnInit() {
  }

  viewItem(products){
    console.log(products);
    this.router.navigate(['/view/'+products.id])
  }

  removeItem(products){
    console.log(products.id);
    this.dataService.removeItem(products.id);
  }

}
