export class Product {
    id: number
    itemName: string
    description: string
    price: number
    imageUrl: string

    constructor(id, itemName, description, price = 0, imageUrl="./../../assets/images/logo.png") {
        this.id = id
        this.itemName = itemName
        this.description = description
        this.price = price
        this.imageUrl = imageUrl
    }
}
