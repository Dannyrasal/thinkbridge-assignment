import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateItemComponent } from './components/create-item/create-item.component';
import { ItemListComponent } from './components/item-list/item-list.component';
import { ViewItemComponent } from './components/view-item/view-item.component';


const routes: Routes = [
  {
    path:"newItem", component:CreateItemComponent
  },
  {
    path:"", component:ItemListComponent
  },
  {
    path:"view/:id", component:ViewItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
